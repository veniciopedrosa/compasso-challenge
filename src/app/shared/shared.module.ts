import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpHelper } from './helpers/http-helper';
import { ToastrModule, ToastrService } from 'ngx-toastr';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
  ],
  providers: [HttpHelper, ToastrService],
})
export class SharedModule {}
