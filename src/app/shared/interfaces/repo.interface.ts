export interface RepoInterface {
  name: string;
  created_at: string;
  html_url: string;
}
