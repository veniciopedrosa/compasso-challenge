export interface UserInterface {
  avatar_url: string;
  name: string;
  login: string;
  bio: string;
  blog: string;
}
