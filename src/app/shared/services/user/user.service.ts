import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { Endpoints } from '../../helpers/endpoints-uri';
import { HttpHelper } from '../../helpers/http-helper';
import { RepoInterface } from '../../interfaces/repo.interface';
import { UserInterface } from '../../interfaces/user.interface';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  constructor(
    private htpp: HttpClient,
    private httpHelper: HttpHelper,
    private toastr: ToastrService
  ) {}

  public getUser(user: string): Observable<UserInterface> {
    const url = this.httpHelper.getUrl(Endpoints.USER, { username: user });

    return this.htpp.get(url).pipe(
      map((data: UserInterface) => data),
      catchError((e: any) => throwError(this.errorHandler(e)))
    );
  }

  public getUserRepos(user: string): Observable<RepoInterface[]> {
    const url = this.httpHelper.getUrl(Endpoints.REPOS, { username: user });

    return this.htpp.get(url).pipe(map((data: RepoInterface[]) => data));
  }

  public getStarredRepos(user: string): Observable<RepoInterface[]> {
    const url = this.httpHelper.getUrl(Endpoints.STARRED, { username: user });

    return this.htpp.get(url).pipe(map((data: RepoInterface[]) => data));
  }

  private errorHandler(error: any): void {
    this.toastr.error(error.statusText);
  }
}
