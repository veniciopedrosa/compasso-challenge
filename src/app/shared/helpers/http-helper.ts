import { Injectable } from '@angular/core';
import { Endpoints, EndpointsURI } from './endpoints-uri';

@Injectable()
export class HttpHelper {
  constructor() {}

  public getUrl(
    uriType: Endpoints,
    pathParams: { [key: string]: string } = {}
  ): string {
    const apiEndpoint = 'https://api.github.com/';
    const uri = EndpointsURI.MAP[uriType];

    return `${apiEndpoint}${this.replaceUrlParams(uri, pathParams)}`;
  }

  private replaceUrlParams(
    url: string,
    pathParams: { [key: string]: string }
  ): string {
    return Object.keys(pathParams).reduce((acc, k) => {
      return acc.replace(`{${k}}`, pathParams[k]);
    }, url);
  }
}
