export enum Endpoints {
  USER = 'user',
  REPOS = 'cast',
  STARRED = 'related',
}

export class EndpointsURI {
  public static readonly MAP: {} = {
    [Endpoints.USER]: 'users/{username}',
    [Endpoints.REPOS]: 'users/{username}/repos',
    [Endpoints.STARRED]: 'users/{username}/starred',
  };
}
