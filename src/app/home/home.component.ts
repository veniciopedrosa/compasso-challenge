import { animate, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { RepoInterface } from '../shared/interfaces/repo.interface';
import { UserInterface } from '../shared/interfaces/user.interface';
import { UserService } from '../shared/services/user/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  animations: [
    trigger('enterAnimation', [
      transition(':enter', [
        style({
          transform: 'translateY(-15%)',
          opacity: 0,
        }),
        animate('500ms', style({ transform: 'translateY(0)', opacity: 1 })),
      ]),
      transition(':leave', [
        style({
          transform: 'translateY(0)',
          opacity: 1,
        }),
        animate('500ms', style({ transform: 'translateY(-15%)', opacity: 0 })),
      ]),
    ]),
  ],
})
export class HomeComponent implements OnInit {
  public control: FormControl = new FormControl();
  public isOpen: boolean;
  public user: UserInterface;
  public ownerRepos: RepoInterface[];
  public starredRepos: RepoInterface[];
  public active: string;

  constructor(
    private service: UserService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.handleInput();

    const { user } = this.route.snapshot.params;

    if (user) {
      this.handleSearch(user);
      this.control.setValue(user);
    }
  }

  public search(): void {
    this.router.navigate(['/', this.control.value]);
    this.handleSearch(this.control.value);
    this.ownerRepos = [];
    this.starredRepos = [];
    this.active = '';
  }

  public handleSearch(user: string): void {
    this.service.getUser(user).subscribe((data) => {
      if (data) {
        this.isOpen = true;
        this.user = data;
      }
    });
  }

  private handleInput(): void {
    this.control.valueChanges
      .pipe(debounceTime(300))
      .subscribe((val: string) => {
        if (val.length < 1) {
          this.isOpen = false;
        }
      });
  }

  public getOwnerRepos(): void {
    this.active = 'owner';

    this.service.getUserRepos(this.control.value).subscribe((data) => {
      this.ownerRepos = data;
    });
  }

  public getStarredRepos(): void {
    this.active = 'starred';

    this.service.getStarredRepos(this.control.value).subscribe((data) => {
      this.starredRepos = data;
    });
  }

  public openRepo(url: string): void {
    window.open(url, '_blank');
  }
}
