# Como utilizar esse projeto

## Clone do projeto

`git clone https://gitlab.com/veniciopedrosa/compasso-challenge.git`

## Executando o projeto

Entre no diretório `cd compasso-challenge` e execute `npm install` para instalar as dependências.

Em seguida execute `npm start`, a aplicação estará disponível em `http://localhost:4200`.

## Utilizando a aplicação

Você pode pesquisar por usuários existentes no Github e ver seus detalhes, assim como seus repositórios públicos e repositórios favoritos.
